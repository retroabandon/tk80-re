Checksum Memory
===============

This does a simple 8-bit checksum of an arbitrary memory range. Load
`start` ($8100) with the starting address, `count` ($8102) with the number
of bytes to checksum, and then set the address to $8004 and run it. The
program will halt with the checksum in A register; press `MON` to return to
the monitor which will display the checksum in digits 5 and 6 of the
display.

On a TK-85 this will end with A=67. (This has not been tested on a TK-80.)

    8200 00 00      start   dw $0000
    8202 10 00      count   dw $0010

    8204 2A 02 82           ld   hl,(count)
    8207 44                 ld   b,h
    8208 4D                 ld   c,l
    8209 2A 00 82           ld   hl,(start)
    820C 3E 00              ld   a,0
    820E 00                 nop
    820F F5                 push  af

    8210 F1         loop    pop  af
    8211 86                 add  a,(hl)
    8212 F5                 push af
    8213 23                 inc  hl
    8214 0B                 dec  bc
    8215 78                 ld   a,b
    8216 B1                 or   c
    8217 C2 10 82           jp   nz,loop
    821A F1                 pop  af
    821B 76                 hlt
                    ; TK-85 halts with A=67 F=$A4=%1010.0100
