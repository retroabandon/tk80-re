TK-80 Games
===========

These are listing files code for fan-made games for the TK-80, as
referenced in the ASCII Publishing book 「復活！TK−８０]. These listings
appear to have been scanned and OCR'd, but not completely checked as
there are errors such as `KEYIN` becoming `KEVIN` in `Moo.lst`.

The games are copyright 1977-2000 Kouichi Kishida+SRA Microcomputer Club.
No license information was given with them.
