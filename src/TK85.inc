;   TK85.inc: standard include header for small TK-85 programs
;   The programs are assembled with Macroassembler AS (asl).

; ----------------------------------------------------------------------

            page 0
            relaxed on              ; accept $nn hex number syntax
            cpu 8085
            z80syntax exclusive

;   We cannot use RIM and SIM in "z80syntax exclusive" mode, but don't want
;   to use mixed syntax ("z80syntax on") because Intel syntax takes
;   precedence, making `jp $xxxx` a "jump on positive" instead of "jump
;   always." So instead we make macros for these 8085 instructions.
;   Note that these must be EXPANDed in order for the opcodes to appear
;   in the byte sequence at the left of the listing.
rim         macro {EXPAND}
            db    $20
            endm
sim         macro {EXPAND}
            db    $30
            endm

; ----------------------------------------------------------------------
;   Hardware

; Intel 8255 Programmable Peripheral Interface
PPI_PA      equ  $F8        ; port A data
PPI_PB      equ  $F9        ; port B data
PPI_PC      equ  $FA        ; port C data
PPI_CR      equ  $FB        ; control register

; ----------------------------------------------------------------------
;   Memory Locations

;   Buffer for character codes to be converted to segements and displayed
;   by `dispchars`. Note reversed order (char8 far left to char1 far right).
char8       equ  $83CF
char7       equ  $83D0
char6       equ  $83D1
char5       equ  $83D2
char4       equ  $83D3
char3       equ  $83D4
char2       equ  $83D5
char1       equ  $83D6

keycode     equ  $83DD  ; last keypress from waitkey/getkey

segments    equ  $83F8      ; leftmost 7-segment display

; ----------------------------------------------------------------------
;   ROM Routines

dispchars   equ  $56C
waitkey     equ  $684   ; wait for keypress
getkey      equ  $690   ; short scan for keypress
