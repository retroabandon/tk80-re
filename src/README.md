Sample and Utility Programs
===========================

File types:
* `*.md` files are "literate" programs that contain hand-assembled (or
  sometimes hand-disassembled) binary code along with a listing.
* `*.i80` and `*.i85` files are assembled with ASL. The 8080 files
  (`*.i80`) are built in both TK-80 and TK-85 versions; the 8085 files
  (`*.i85`) are built for TK-85 only. The output binaries and tidied-up
  listing files are placed under `bin.tk80/` and `bin.tk85` directories
  respectively and committed. (The listings are useful for keying in
  programs by hand.)
* `*.inc` files are include files to help set up the ASL assembler for a
  particular platform. Use e.g. `include "src/TK85.inc"` to use them
  in your programs.

Include Files:
* `TK80.inc`, `TK85.inc`: Standard header and definitions for TK-80 and
  TK-85 programs. These allow Motorola-style hex notation (`$xxxx`). The
  programs are assembled for the 8080 or 8085 CPU, but source code must use
  Zilog mnemonics.

### Programs

Program address summary:

    start last  program
    ────────────────────────
    8000  8020  anim
    8200  821B  cksum
    8220  823B  charset
    8240  825E  showkey

* [`anim.md`]: Animation of line moving across display.
  Taken from a YouTube video, but probably not original to him.

* [`charset.i85`]: Display TK-85 character set, 8 chars at a time.
  Press any key (except `MON`) to show the next 8 chars.

* [`cksum.md`]: Simple program to do an 8-bit checksum of an arbitrary memory
  range. Useful for checking that your ROM is the same as one of the
  versions here. Known ROM checksums are in the file.

* [`hello.md`]: Very much what it sounds like.

* [`showkey.i85`]: Display keypad scan codes in the rightmost two
  characters of the display. `MON` will exit.



<!-------------------------------------------------------------------->
[`anim.md`]: ./anim.md
[`charset.i85`]: ./charset.lst
[`cksum.md`]: ./cksum.md
[`hello.md`]: ./hello.md
[`showkey.i85`]: ./showkey.lst
