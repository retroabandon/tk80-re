;   TK80.inc: standard include header for small TK-80 programs
;   The programs are assembled with Macroassembler AS (asl).

; ----------------------------------------------------------------------

            page 0
            relaxed on              ; accept $nn hex number syntax
            cpu 8080
            z80syntax exclusive

; ----------------------------------------------------------------------
;   Hardware

; Intel 8255 Programmable Peripheral Interface
PPI_PA      equ  $F8        ; port A data
PPI_PB      equ  $F9        ; port B data
PPI_PC      equ  $FA        ; port C data
PPI_CR      equ  $FB        ; control register

; ----------------------------------------------------------------------
;   Memory Locations

keycode     equ  $83F3  ; last keypress from waitkey/getkey

segments    equ  $83F8      ; leftmost 7-segment display

; ----------------------------------------------------------------------
;   ROM Routines

waitkey     equ  $216   ; wait for keypress
getkey      equ  $223   ; short scan for keypress
