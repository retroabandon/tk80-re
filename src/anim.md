TK-80/85 Animation Demo
=======================

Simple animation of horizontal line going across display.
From video [NEC ワンボードマイコン「TK-85」の紹介＆動作確認][vid].

This has been tested on the TK-85, but should work on the TK-80 as well
(though that is untested).

    8000: 21 F8 83 3E 08 36 40 CD
    8008: 14 80 36 00 23 3D C2 05
    8010: 80 C3 00 80 16 40 06 00
    8018: 05 C2 18 80 15 C2 16 80
    8020: C9

Disasembly:

    8000: 21 F8 83      ld   hl,$83F8   ; leftmost 7-segment display
    8003: 3E 08         ld   a,$8
    8005: 36 40         ld   (hl),$40   ; middle segment?
    8007: CD 14 80      call $8014
    800A: 36 00         ld   (hl),$00   ; all segements off
    800C: 23            inc  hl
    800D: 3D            dec  a
    800E: C2 05 80      jp   nz,$8005
    8011: C3 00 80      jp   $8000

    8014: 16 40         ld   d,$40      ; delay loop: 64 × 256 = 16384
    8016: 06 00         ld   b,$00
    8018: 05            dec  b
    8019: C2 18 80      jp   nz,$8018
    801C: 15            dec  d
    810D: C2 16 80      jp   nz,$8016
    8020: C9            ret



<!-------------------------------------------------------------------->
[vid]: https://www.youtube.com/watch?v=8loGh_amABo
