TK-85 Vendor Manuals and Documentation
======================================

These are scans of the original documentation that came with the TK-85.

* `NEC TK-85 Training Book_text.pdf`: OCR'd and size-reduced scan of the
  NEC [_TK-85 Training Book_][tk85tb], 1980. For higher-resolution scans,
  see the item on archive.org.

* `NEC TK-85 Schematics.pdf`: Schematics and board layout diagrams
  extracted from the NEC _TK-85 Training Book._



<!-------------------------------------------------------------------->
[tk85tb]: https://archive.org/details/NEC_TK-85_Training_Book/NEC%20TK-85%20Training%20Book/
