TK-80 and TK-85 Parts Datasheets
================================


TK-85
-----

#### ICs

- NEC [μPB8212C]: Eight-Bit Input/Output Port: Address bus A0-A7 latch.

#### Discrete

- NEC [1SS53] switching diode: MON and RESET switch debounce circuits.



<!-------------------------------------------------------------------->
<!-- TK-85 ICs -->
[μPB8212C]: ./NEC_μPB8212C.pdf

<!-- TK-85 Discrete -->
[1SS53]: ./1SS53-1SS54-1SS55.pdf
