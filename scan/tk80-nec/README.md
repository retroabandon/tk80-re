TK-80 Vendor Manuals and Documentation
======================================

These are scans of the original documentation that came with the TK-80.

* `Com80.pdf` μCOM-80ユーザーズ・マニアル (_μCOM-80 Users Manual_):
  Description and programming of the μPD8080A microprocessor, including
  differences from the Intel 8080A, and a list of other related ICs.

* `Tk80.pdf`: μCOM-80トレーニング・キット TK-80ユーザーズ・マニアル
  (_μCOM-80 Training Kit TK-80 Users Manual_):
  Manual for the TK-80 training board, including hardware and software
  description.

* `Circuit.pdf`: 付図II TK-80回路図 (_Appendix II: TK-80 Schematic_):
  This is the fold-out page 145 not included in `Tk80.pdf`.

* `Silk-screen.pdf`: 付図III プリント基板部品面図 (_Appendix III: PCB Diagram_):
  This is the fold-out page 147 not included in `Tk80.pdf`.
