NEC TK-80, TK-85
================

The NEC TK-80 (8080 CPU, 1976) and TK-85 (8085 CPU, 1980) are single-board
"trainer" systems with a hex keypad and display. Photos of and information
about the TK-85 can be found at [NEC TK-85], and further hardware
information here is in the [`docs/`](./docs/) directory.

### Files

Documenation:
- `scan/`: Scans of historical documents.
  - `scan/brochure/`: Copies of brochures.
  - `scan/datasheet/`: Datsheets for TK-80 and TK-85 parts.
  - `scan/tk80-nec/`: Vendor documentation for the TK-80.
  - `scan/tk85-nec/`: Vendor documentation for the TK-85.
- `doc/`: Documentation originating in this repo
- `img/`: Pictures of hardware.

Code:
- `rom/`: ROM BIOS binaries, disassembly and source.
- `Build`: Build script for `rom/` and `src/` (see below).
- `src/`: Programs for the TK-80 and TK-85. Some (in `.md` files) are straight
  hex type-ins, others are source code to be assembled with ASL (see below).
- `bin.tk80/`, `bin.tk85/`: Binaries and listings of assembled files
  from `src/` above.

Other:
- `emu/`: Emulator documentation (in [`emu/README.md`]) and support files.
- `NonTK/`: Other non-NEC 8080 and 8085 trainers, for comparison.

If you simply want to play with the emulated machines, have a look at
[`emu/README.md`] and the `emu/emu` script.


Building Software
-----------------

The top-level `./Build` script does the following:
- Disassembles the original ROM binaries and rebuilds them (excepting on
  Windows).
- Assembles the reverse-engineered source code of the ROMs and compares
  them with the original ROMs.
- Assembles the source code under `src/`.

The disassembly is done with `z80dasm`, which is available in most Linux
distributions but not for Windows. The build script will automatically skip
the disassembly stage on Windows systems by detecting MINGW.

Assemblies produce `.bin` and `.lst` files that is normally commited under
`bin.*/`; ensure you re-build and commit these with any source changes. The
assembly is done with [Macroassembler AS][asl] (`asl`); if this is in your
path that will be used, otherwise it will look in a few obvious places to
try to find it.

The easiest ways to get Macroassembler AS are:
- Linux:
  - Run `./Build -A`, which uses the [t8dev] toolkit to build it for you.
  - Build from your own preferred version of the source fetched from the
    [`asl-releases`] repo on GitHub and install it to `/opt/asl/` or
    `/usr/local/`.
- Windows: download the Win32 binary release from the [ASL downloads
  page][asl-dl] and unpack it under `C:\Program Files (x86)\asl\`.

The [ASL manual][aslman] is available online.

To see a diff of errors when comparing generated ROM binaries to the
original you will also need [`meld`][]; this is also available in most
Linux distributions, and downloadable for Windows.



<!-------------------------------------------------------------------->
<!-- Building Software -->
[`asl-releases`]: https://github.com/Macroassembler-AS/asl-releases
[`emu/README.md`]: ./emu/README.md
[`meld`]: https://meldmerge.org/
[asl-dl]: http://john.ccac.rwth-aachen.de:8000/as/download.html
[asl]: http://john.ccac.rwth-aachen.de:8000/as/
[aslman]: http://john.ccac.rwth-aachen.de:8000/as/as_EN.html
[t8dev]: https://github.com/mc68-net/t8dev
