NEC TK-80BS Monitor
===================

The TK-80BS Monitor is entirely separate from the standard TK-80 monitor
and uses the keyboard and video display, rather than the hex keypad and
numeric LED display.

The information here is for the [80BS-clone] ROMs, especially the
[`BSMONITOR.TXT`] file distributed with version 3. It's a clean-room
re-implementation of the original NEC ROMs, and so while it matches
publicly available information about the NEC ROMs (including some of the
entry points and work area addresses), it may not be exactly the same.


Commands
--------

Note that the literal commas below are required. Notes marked ★ are
extensions in the 80BS-clone ROMs and not available in the original NEC
ROMs.

Ranges are from `start` to `last` _inclusive_ of `last`.

- `BA`: Enter BASIC.
- `B1`: ★ Enter level 1 BASIC.
- `BY`: Leave BS monitor for standard keypad/LED monitor.

Register/memory examine/change:
- `DR`: Display registers.
- `CR`: Change registers. Similar to `CM` below.
- `CM,addr`: Change memory starting at address _addr_ (default $0000). The
  memory location and current value at that location will be displayed.
  - A 1 or 2 digit hex value followed by Enter will store that value in the
    current location and move to the next location.
  - A space followed by Enter will preserve the value at the current
    location and move to the next location.
  - ★ `X` followed by Enter will move back to the previous location.
  - Enter alone will exit change memory mode.
- `DM,start[,last]`: Display memory
  from _start_ (default $0000) through _last_ (default _start_+15).
  - ★ The display will pause when the screen is full. Press space to
    continue the display or Enter to abort the printout.
- `MM,start,last,dest`: Move memory.
- `GO,addr`: run code starting at _addr._ _Addr_ defaults to current PC.
  Executing `ret` appears to return to monitor.

Breakpoint control (clone ROM is somewhat different from original):
- `BP,addr,count`: Breakpoint.
  - ★ `count` not available in clone ROM
  - ★ `BP,,icount` switches to interrupt mode after specified number of
    interrupts. (See docs for details.)
- `RT`: return from breakpoint. (★?) Also Enter does the same.
- `OF`: switch from interrupt to normal mode

Screen controls:
- `CL`: clear screen
- `CC`: "change contrast, switching between black-on white and white-on black.

Tape:
- `ST,start,last`: Save memory to tape. At `READY?` type `Y` to continue.
- `LT`: load from tape
- `CT`: compare tape to memory



<!-------------------------------------------------------------------->
[80BS-clone]: ../rom/80BS-clone
[`BSMONITOR.TXT`]: ../rom/80BS-clone/etkroms3/BSMONITOR.TXT
