TK-80/85 Hardware
=================

References:
- \[tk80um] μCOM-80トレーニング・キットTK-80ユーザーズ・マニアル
  (_μCOM-80 Training Kit TK-80 Users Manual_).
- \[tk85tr] _TK-85 Training Manual_


Power
-----

The TK-80 requires +5 V 1 A and +12 V 150 mA (with all display segments
lit), and this is normally supplied via the edgeboard connector, per the
diagram in [[tk80um]] p.37/P.42. +12V should not come up before +5V, and
should not come down after +5V.

The TK-85 requires 5V only, and generally draws 0.7 – 0.8 A.


Peripherals
-----------

All _'nn_ references below are to _74LSnn_ parts.

### Display

A DMA system reads RAM at $83F8 through $83FF and sets segments based on
the bits:

         $01
     $20 |_|$02     $40 middle bar
     $10 |_|$04     $80 decimal point
         $08

For the TK-80 only, the DMA and display is controlled by 8255 port C bit 7
(1=enable, 0=disable). The display is disabled by the ROM code when it
does bit-banged serial I/O (port B, bit 0) to avoid variance in the timing
loops. (Port C bit 7 is used for single-step control on the TK-85.)

The DMA timing control is different between the two:
- TK-80:
  - A 555 timers generates regular pulses to trigger the DMA.
- TK-85:
  - CPU `CLK` drives '93 counter ÷2 stage, '74 flip-flop, and gates to
    produce the `D̅M̅A̅C̅` signal (schematic CPU page).
  - `D̅M̅A̅C̅` drives a 74LS161 counter and some gates to produce the `DMA` and
    `D̅M̅A̅` signals.
- Both:
  - The 555 output drives a "223" / `D̅M̅A̅` drives a '93 ÷8 counter.
  - The 223 / '93 outputs generate the address from which to read the
    segment data and enable the corresponding digit via a 2155 / '155 that
    drives the segment display transistors.
  - `D̅M̅A̅` enables an 8212 I/O port on the data bus to capture the segment
    data.

### Keyboard (TK-85)

The keyboard is 3 rows of 8 keys, plus the `MON` key which generates an
NMI. The rows are selected by bringing 8255 port C ($FA) bit 4, 5 or 6 low
for rows 0, 1 and 2 and then reading the column from port A ($F8).

        Column/PA bit:   7    6    5    4    3    2    1    0
    ─────────────────┬─────────────────────────────────────────
                     │ LOAD STOR WINC RINC RDEC ADDR  RET  RUN  (TK-80)
    Row 2 / PC bit 6 │  REG MODE WR/E RINC RDEC ADDR CONT  RUN  (TK-85)
    Row 1 / PC bit 5 │   F    E    D    C    B    A    9    8
    Row 0 / PC bit 4 │   7    6    5    4    3    2    1    0

The `waitkey` ($684) and `getkey` ($690) routines will scan the keyboard,
returning the scanned character in A.

### 8255 PPI

(TK-85) The NEC D8255AC-5 is on ports $F8-$FB (PA, PB, PC, CR); possibly
mirrored elsewhere as well. It's connected as follows.

    Port  Lines   Dir   Connection
    $F8  PA0-PA7  in    keypad column sense; external pull-up
    $F9  PB0-PB7  in    unused; on edge connector
    $FA  PC0-PC3  out   unused; on edge connector
    $FA  PC4-PC6  out   keypad row drive (pulls down only; diode stops pull up)
    $FA    PC7     "    IC34 'LS175 pin 1 /CLR input (for single-step)

`Dir` above indicates the default direction configured set up by the
monitor on reset or NMI (i.e., every time you press `MON`). This is set by
writing the control register ($FB) with $92 (%10010010) to for Basic I/O
mode. Change bit 1 to 0 ($90) to change port B to output.

Reads from the control port, at least via the monitor, do not seem to work,
always returning $FF.

### Edgeboard (Card Edge) Connector

2×50P at 0.125"/3.18mm pitch; see [repair-mod] for compatible part
numbers). From left to right, A1-A50 across the top, B1-B50 across the
bottom. Positions marked `-` have fingers on the edge connector that bring
the signal on to the board; blank do not have fingers. [[tk85tr]] p.184.

The data bus is not available on the edgeboard connector unless the two
optional [μPB8216] bus drivers are installed in the IC24/IC25 sockets. See
[`8216-clone/`](8216-clone/README.md) for more notes and a substitute for
these.

Even for the buffered signals, fanout is limited and address and data buses
may need additional buffering.

                   ┌────────┐         left side / power connectors
               GND │B1    A1│ GND
               GND │B2    A2│ GND
               +5V │B3    A3│ +5V
                 - │B4    A4│
              +12V │B5    A5│ +12V      +12V is TK-80 only
                 - │B6    A6│ /ALE
                 - │B7    A7│
                 - │B8    A8│ /RD
             IO/!M │B9    A9│ /WR
               AB7 │B10  A10│ AB15
               AB6 │B11  A11│ AB14
               AB5 │B12  A12│ AB13
               AB4 │B13  A13│ AB12
               AB3 │B14  A14│ AB11
               AB2 │B15  A15│ AB10
               AB1 │B16  A16│ AB9
               AB0 │B17  A17│ AB8
                   │B18  A18│
                   │B19  A19│
             /MEMR │B20  A20│
             /MEMW │B21  A21│
                   │B22  A22│ READY
                   │B23  A23│
                   │B24  A24│
             /HLDA │B25  A25│ /HOLD
               DB7 │B26  A26│
               DB6 │B27  A27│ DMA
               DB5 │B28  A28│ /DMA
               DB4 │B29  A29│ /DBSL
               DB3 │B30  A30│ PB0
               DB2 │B31  A31│ PB1
               DB1 │B32  A32│ PB2
               DB0 │B33  A33│ PB3
           /RST5.5 │B34  A34│ PB4
           /RST6.5 │B35  A35│ PB5
                 - │B36  A36│ PB6
                 - │B37  A37│ PB7
                 - │B38  A38│ PC0
                 - │B39  A39│ PC1
                 - │B40  A40│ PC2
                 - │B41  A41│ PC3
             /INTR │B42  A42│ /CS3
             /INTA │B43  A43│ /CS2
         RESET_OUT │B44  A44│ /CS1
        /RESET_OUT │B45  A45│ /RESET_IN
                   │B46  A46│
                   │B47  A47│
             S0⋅S1 │B48  A48│ CLK
             S0+S1 │B49  A49│
               GND │B50  A50│ GND
                   └────────┘         right side / keypad

Notes:
- `S0⋅S1`: CPU opcode fetch signal
- `S0+S1`: CPU halt signal
- `AB15`–`AB0`: address bus (buffered)
- `DB7`–`DB0`: data bus (buffered via optional 2× μPB8216, IC24 and IC25)
- `/CS1`–`/CS3`: RAM expansion chip selects ($8400, $8800, $8C00-$8FFF)
- `DMA`/`/DMA`: indicates DMA transfer period
- `/DBSL`: external data bus selected


Address Decoding
----------------

The TK-85 has an NEC μPB8212C Eight-Bit Input/Output Port near the upper
left of the board. This has tri-state capability, 8 GPIOs which must all be
input or output, and mode, strobe, interrupt and clear lines. This is used
to latch the CPU's A0-A7 signals, which are multiplexed on the same pins
as D0-D7.



<!-------------------------------------------------------------------->
[repair-mod]: ./repair-mod.md
[μPB8216]: ../datasheet/nte8216.pdf
[tk80um]: ./doc/tk80man/Tk80.pdf
