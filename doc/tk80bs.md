TK-80BS (BASIC STATION) Expansion Board
=======================================

This is a single board that is about the same size as the TK-80 itself,
also with a 100-pin edgeboard connector, that connects to the TK-80
with a small two-slot backplane.

The board includes the following:
- 1× [8251] USART (for CMT?)
- 1× [8255] Programmable Peripheral Interface (for keyboard?)
- 2× 8112 8-bit I/O ports
- 4× 2332 4K ROM sockets:
  - $F000-$FFFF (BS monitor)
  - $E000-$EFFF (Level 1/2 BASIC)
  - $D000-$DFFF (Level 2 BASIC; unpopulated for Level 1)
  - "CHARA. GENE." character generator ROM
- DRAM

(More for video output, etc.)



<!-------------------------------------------------------------------->
[8251]: https://en.wikipedia.org/wiki/Intel_8251
[8255]: https://en.wikipedia.org/wiki/Intel_8255
