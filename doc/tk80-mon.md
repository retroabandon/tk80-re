NEC TK-80 Monitor
=================


Internals
---------

The following ROM entry points are "well known" in the sense that they
have been described by various sources.

- $1A1: Display current address and data on the left and right hand sides
  of the LED display. ♣all. [[nkomatsu]]
- $1C0: Display contents of 4-byte numeric buffer as 8 digits on segement
  displays. ♣all. [[nkomatsu]]
- $216 `waitkey`: Wait for a key to be pressed and return it. ♣AFBDE.
  [[nkomatsu]]
- $2DD delay3.38: Spin delay. 4.5112 ms according to [[nkomatsu]]. ♣DE.
- $2EA delay6.75: Spin delay. 9.0171 ms according to [[nkomatsu]]. ♣DE.
- $2EF delay20.2: Spin delay. 27.176 ms according to [[nkomatsu]]. ♣DE.


References
----------

- \[nkomatsu], [TK-80][nkomatsu]. Describes the board hardware and
  software, and gives a few addresses of ROM routines.



<!-------------------------------------------------------------------->
[nkomatsu]: http://www.st.rim.or.jp/~nkomatsu/evakit/TK80.html
