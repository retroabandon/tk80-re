NEC TK-80 and TK-85 Hardware
============================

### Specifications

            TK-80 (1976)            TK-85 (1980)
    CPU     μPD8080AD (2MHz)        μPD8085A (2.4576MHz)
    ROM     768b μPD454D (max 1K)   2K 2716 (1 socket used, 3 free)
    RAM     512b (max 1K)²          1K (4K w/external²)
    Power   +5V 1.1A, +12V 0.15A    +5V 1.5A (actually pulls about 0.8A)
    Size    310 × 180 mm            310 × 220 mm

    ¹ 512b RAM is in sockets 3,4,7,8 for $8200-$83FF;
      expansion RAM in sockets 1,2,5,6 for $8000-$81FF.
    ² External chip selects provided for three pairs of 2114 4×1024 RAMs.

#### TK-80 Hardware Notes

The NEC μPD8080A processor is [slightly different][μC80] from the Intel
8080. (This was replaced in 1979 with the μPD8080AF that is completely
compatible with the Intel 8080A in all details; that was used in the TK-80E
model onwards.) The differences are:
- BCD adjust is supported for subtraction as well as addition.
- Flag bit 5 (unused on the 8080) is a `SUB` flag, similar to the Z80's
  `N` flag at bit 1.
- `MOV r,r` takes only 4 clock cycles instead of 5.
- 3-byte instructions are allowed in an interrupt acknowledge cycles so
  that as well as `RST`, `CALL` instructions can also be used.

The TK-80 RAM devices are CMOS μPD5101E and will hold their data with only
3 V of standby power which can be supplied by a pair of alkaline batteries
connected to the "EXT Vcc" terminals. The "RAM" switch should be set to the
"PROTECT" position to disable the chip selects before removing main power.
These CMOS chips however were slower than the pin-compatible NMOS 2101
devices, and thus the board had a wait state inserted for memory access.

#### TK-85 Hardware Notes

The NEC D2316EC ROM is probably a 2316 mask ROM also programmed to have
a negative-logic enable on pin 18, as a 2716 does, rather than the more
usual positive logic enable of a 2316, because people putting in their
own ROMs are most likely to be using EEPROMs.

### Models and Release Dates

- 1976-08: TK-80 (¥88,500).
- 1977-12: TK-80E (¥67,000): Cost-reduced version; μPD8080AFC processor
  (Intel incompatibilities removed); μPD454D EEPROM replaced with μPD464
  mask ROM (same pinout).
- 1978-12: TK-80BS (¥128,000): "Basic Station" expansion kit including
  expansion board with BASIC in ROM, more RAM, video output, and keyboard.
  [[80BS]]
- 1979-04: COMPO-BS/80: TK-80BS integrated into a case, with power supply.
- 1979-05: PC-8001 (¥168,000): Z80 personal computer.
- 1980-05: TK-85 (¥44,800): 8085 CPU, CMT interface, etc.

Sources: [s-oga's TK-80 page]; [ja Wikipedia][wj].

### Official Documentation

[This blog entry][giikoh] gives an overview of the official NEC TK-80
documentation, and photos of the covers and a few internal pages.

Manuals:
- TK-80ユーザーズ・マニュアル (147 pp.)
- TK-80アプリケーション・ノート (177 pp.)
- TK-80応用プログラム (53 pp.)
- μCOMシリーズ 総合ユーザーズガイド (118 pp.)
- μCOM-80 ユーザーズ・マニュアル (178 pp.)
- μCOM-8/80 プログラムライブラリ（No.1～No.3）
- μCOM-8/80 プログラムライブラリ（No.4～No.11） (76 pp.)
- μCOM-8/80 プログラミング入門 (119 pp.)

Datasheets:
- μCOM-8/80 インストラクション活用法
- μPD8080Aファミリ　CPU　データシート／8ページ
- μPB8212C/D　I/O PORT　データシート／12ページ
- μPB8228C/D・μPB8238C/D　バスドライバー　データシート／10ページ
- μPD8255C　周辺インターフェース　データシート／24ページ
- μPD454D　EEPROM　データシート／8ページ
- μPD2101ALC　S-RAM　データシート／6ページ
- μPD5101C/E 　S-RAM　データシート／8ページ


References
----------

- NEC, [TK-85 Training Book][tk85tb], 1980. The manual that came with the
  TK-85, including detailed hardware information and schematics.
- 榊正憲, 「復活TK-80」 (Masanori Sakaki, _TK-80 Revivial_), ASCII
  Publishing, 2000-04-01. ISBN 4-7561-3401-7. A non-technical explanation
  and history of the TK-80. Includes a CD-ROM with scans of the original
  NEC documentation, an emulator, and some games. Out of print.
- [TK-85 メモリ増設][tk85-memexp]: Memory expansion for TK-85.
- [ＭＹＣＰＵ８０（ＴＫ８０回路）操作説明書][mycpu80-man]: Manual for a
  board similar to the TK-80. Chapters 2 and 3 cover monitor usage, and
  there's a monitor listing at the end. The LEDs output registers are in a
  different location, but presumably the keyboard described there is at the
  same I/O addresses as on the TK-85.
- [ND80ZIII][nd80]: Z80-clone of the TK-80.
  - [Schematics][nd80-sch]. Some differences from TK-80/85 due to Z80
    instead of 8080/8085 and different display buffer location.
    Theory of Operation given here and in subsequent posts.
  - [Display circuit schematic][nd80-disp]: DMA from memory, moved to
    original TK-80 location $83F8-$83FF.
  - [TK-80 step circuit schematic][nd80-tk80step]. When set up, monitor
    does a RET into user program which executes one instruction just as
    this circuit generates an INT.
- [中日電工][cndk] (Chuu-nichi Electric Works): Stuff above and other
  related things.
- [NEC TK-85 プログラムのCD化][itoda] "Saving TK-85 programs to CD."
  - Recording into Audacity. Tape frequencies.
  - Program listings (from TK-85 manual?): LED display animation and
    digital timer.
  - Link to scan of [JMC TK-85 I/O Board brochure][itoda-io].

### Related Resources

- Intel produced a similar trainer called the [SDK-85] or MCS-85.
  - [User's Manual][sdk85um], including monitor listing.
  - [ForNextSoftwareDevelopment/8085][sdk85fn] is an assembler,
    disassembler and SDK-85 simulator (C#, Windows). it includes
    machine-readable source of monitors 1.2 and 2.1 and the AP-29 CMT I/O
    routines. The [author][fnsw] is on the VCFED forums.



<!-------------------------------------------------------------------->
[80BS]: https://support.nec-lavie.jp/support/product/data/spec/acb/a688-1.html
[giikoh]: http://giikoh.livedoor.blog/archives/22989980.html
[s-oga]: http://web.kyoto-inet.or.jp/people/s-oga/tk80/index.html
[wj]: https://ja.wikipedia.org/wiki/TK-80
[μC80]: https://en.wikipedia.org/wiki/NEC_%CE%BCCOM_series#%CE%BCCOM-80

<!-- References -->
[NEC TK-85]: https://www.funkygoods.com/garakuta/tk_85/tk_85.htm
[cndk]: https://userweb.alles.or.jp/chunichidenko/
[itoda-io]: http://itoda.server-shared.com/TK85IO.pdf
[itoda]: http://itoda.server-shared.com/tk85.htm
[mycpu80-man]: https://userweb.alles.or.jp/chunichidenko/mycpu80setumeisyo/mycpu80_tk80sousa.pdf
[nd80-disp]: https://userweb.alles.or.jp/chunichidenko/mycom_tukuro35.html
[nd80-sch]: https://userweb.alles.or.jp/chunichidenko/mycom_tukuro3.html
[nd80-tk80step]: https://userweb.alles.or.jp/chunichidenko/mycom_tukuro24.html
[nd80]: https://userweb.alles.or.jp/chunichidenko/mycom_tukuro_mokuji1.html
[tk85-memexp]: http://www2b.biglobe.ne.jp/~houmei/rd/tk85.html
[tk85tb]: https://archive.org/details/NEC_TK-85_Training_Book/NEC%20TK-85%20Training%20Book/

<!-- Related Resources -->
[fnsw]: https://forum.vcfed.org/index.php?threads/intel-8085-question.1238878/post-1268073
[sdk85]: https://en.wikipedia.org/wiki/Intel_system_development_kit#SDK-85
[sdk85fn]: https://github.com/ForNextSoftwareDevelopment/8085
[sdk85um]: http://www.bitsavers.org/components/intel/8085/9800451B_SDK-85_Users_Man_Feb80.pdf

