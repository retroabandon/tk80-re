TK-80/85 Expansion Boards and Accessories
=========================================

The __NEC-100BUS__ 5-slot rack has 5 100-pin edgeboard connectors for
connecting TK-80 boards. [[keikato]]

The NEC __TK-M20K__ was a standalone expansion board, but seems to have
often been used in the BS and similar systems. It apparently also worked on
the TK-85 as well. [[ssplusone]] [[keikato]]
- 12 KB RAM: 24× μPD2114 (fully populated); fills $A000-$CFFF
- 8× sockets for 1 KB μPD458 EPROM. (Same as 2708? Some say EEPROM.)
- 2× μPD8255 Programmable Peripheral Interface
- 1x μPD8251 USART

<img src="../img/tkm20k1.jpg" width="200"/>
<img src="../img/tkm20k2.jpg" width="200"/>
<img src="../img/tkm20k3.jpg" width="200"/>



<!-------------------------------------------------------------------->
[ssplusone]: https://www.ssplusone.com/マイクロコンピュータ/tk-80の想い出/tk-85/
[keikato]: http://keikato.cocolog-nifty.com/blog/2016/09/tk-80tk-m20k-82.html
