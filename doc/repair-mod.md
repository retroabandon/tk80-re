TK-80/85 Repairs and Modifications
==================================

Common Parts
------------

- __USB Power:__ the board draws around 0.8 A, so most USB power addapters
  will work fine. Chop a USB cable in half to connect to the power posts;
  optionally crimp "dupont" male pins to the end. In most cases USB cables
  use a red wire for +5 V and a black wire for ground, but confirm with
  your particularl cable.
- __Edgeboard connector:__ 2×50P at 0.125" (3.18mm) pitch. A Sullins
  EBA50DRTH (Digikey [EBA50DRTH-ND], ~$12) has been tested to work. (It's a
  tight fit; don't expect to be disconnecting and reconnecting it a lot.)
  This has male pins for PCB mounting, but single female "dupont"
  connectors seem to plug on to the pins just fine, and even a 2P dupont
  can be connected to upper and lower pins. (Dupont pitch is to narrow to
  connect a 2P or larger connector to adjacent pins on the same row.)
- __Standoffs:__ There are nine positions for stand-offs: four under the
  keypad, two next to the keypad and three at the other corners of the
  board. In the original configuration, the lower left corner of the keypad
  is is unused, and the position next to the upper right corner of the
  keypad is unused.
  - The non-keypad standoff assemblies are a 25m M3 bolt through a metal
    washer, the PCB, a 15 mm plastic sleeve and into a rubber-encased metal
    nut serving as the foot.
  - The keypad standoff assemblies have the same bolt coming up through the
    bottom of the rubber foot (without a nut in it), through the plastic
    sleeve and into the metal keypad frame, which is M3 threaded.
  - The standoffs are are far taller than necessary and can be replaced
    with standard M3 6mm standoffs (preferably plastic rather than brass).
    Keypad positions can use a single female-male standoff; other positions
    need a screw from the top through the washer and board to a
    female-female standoff. (Re-use the original washers to minimise the
    chance of damage to the PCB solder mask.)

TK-85 Parts
-----------

- __Display transistors__ (TR2–TR8): The NEC 2SA952 is not easily
  available, but the S8550 is common, has slightly better specifications,
  and has been tested to work fine. Note that the pinout is slightly
  different: EBC instead of BCE. This still fits fine, though: rotate the
  S8550 slightly to the left and then bend the E pin over a bit.

- __μPB8216 Bus Drivers__ for Edgeboard Expansion Connector. See
  [`8216-clone/`](8216-clone/README.md) for more notes and a substitute for
  these.



<!-------------------------------------------------------------------->
<!-- Parts -->
[EBA50DRTH-ND]: https://www.digikey.com/en/products/detail/sullins-connector-solutions/EBA50DRTH/973832
