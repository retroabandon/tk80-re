NEC TK-85 Monitor
=================

Usage
-----

If you accidentlly trash the monitor workspace, it can get into strange
states that cause mysterious behaviour. If you're encountering this, it's a
good idea to reset the machine (using the reset button) when things seem
weird. This will not clear memory, except for the monitor's workspace RAM.

### Entering the monitor

- While a program is running, `MON` will stop the program (saving the
  `CONT` address) and display the location in the left four digits
  and the AF register in the right four digits. When already in the
  monitor, it will reset the current examine/deposit address to $0000
  (the `CONT` address is unchanged)

### Running programs

- Set the mode switch to `AUTO` for continuous program execution or to
  `STEP` for single-step execution (going back to the monitor after every
  instruction).
- `RUN` starts execution at the address shown on the left-hand side of the
  display. (Set this by entering an address and using `ADRS SET` or using
  the `READ INC`/`READ DEC` keys.)
- `CONT` continues execution from where it left off after pressing `MON`
  or re-entering monitor from single-step or a breakpoint.

### CMT Load/Save

- `Mode` → `A/SAVE`
  - Enter 8-bit file number ($00-$FF) → `WR` → display clears
  - start address → `WR`, moved to left four digits
  - last address (inclusive) → `WR`, stays in right four digits, save starts
  - right-hand digit segments blink fast (lowest segment) to 1 Hz (decimal
    point) while saving
  - on completion, display shows `S-nn` where _nn_ is the file number
- `Mode` → `B/LOAD`
  - Enter 8-bit file number ($00-$FF) → `WR` → load starts
  - right-hand digit segments blink fast (lowest segment) to 1 Hz (decimal
    point) while loading
  - At end, curaddr is start addr of load, curdata is last addr loaded

### Memory test

- Press`MODE` then `TM`, enter start-addr, `WR/ENT`,
  end-addr (inclusive), `WR/ENT`, and test will start.
- Test can take several seconds even for just $100 bytes of memory.
  When done, Will display `9ood` ("good") on screen. Otherwise the first
  failing address is displayed, followed by a code of some sort.
- Seems to leave $FF in the first location, $00 in the second, etc.
- Testing $8000-$839F works; beyond that it will stomp on monitor
  housekeeping data.

### Terminating Programs

- `HLT` $76 will halt all processing; the monitor can be re-entered with `MON`.
- `RST 0` $C7 will reset the machine to its intial state (but preserve memory).


Internals
---------

ROM entry points:

    $0000   RST 0 ($C7): cold start (preserves memory)
    $0008   RST 1 ($CF): jp $83B1
    $0018   RST 2 ($D7): jp $83B4
    $0018   RST 3 ($DF): jp $83B7
    $0028   RST 4 ($E7): jp $83BA
    $0028   RST 5 ($EF): jp $83BD
    $0038   RST 6 ($F7): jp $83C3
    $0008   RST 7 ($FF): jp $83C6

RAM work area:

    83FF    7-segment display (rightmost)
    83F8    7-segment display (leftmost)

    83F2    BR.D (lsb, msb)
    83F0    BR.P (lsb, msb)

    83EB    A register
    83EA    F register (bits 7-0: SZ-H-P-C)
    83E9    B register
    83E8    C register
    83E7    D register
    83E6    E register
    83E5    H register
    83E4    L register
    83E3    SP register MSB
    83E2    SP register LSB
    83E1    PC register MSB
    83E0    PC register LSB

    83C6    RST 7 target address
    83C3    RST 6 target address
    83BD    RST 5 target address
    83BA    RST 4 target address
    83B7    RST 3 target address
    83B4    RST 2 target address
    83B1    RST 1 target address

    838F    default stack base


References
----------

- [ＭＹＣＰＵ８０（ＴＫ８０回路）操作説明書][mycpu80-man]: Manual for a
  board similar to the TK-80. Chapters 2 (p.7) and 3 (p.13) cover monitor
  usage.



<!-------------------------------------------------------------------->
[mycpu80-man]: https://userweb.alles.or.jp/chunichidenko/mycpu80setumeisyo/mycpu80_tk80sousa.pdf
