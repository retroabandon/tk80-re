μPB8216 Bus Drivers for Edgeboard Expansion Connector
=====================================================

The data bus is not brought out directly to the expansion connector pins
B26-B33, and won't be available there unless you have the optional pair of
[μPB8216] bus drivers installed in the IC24/IC25 sockets. These are
difficult to find these days, but they have been seen for around $4-7 each
on Ebay.

On the 8216 the The `DOn` and `DIn` pins are tied together and connect to
the CPU `ADn` pins. The `DB7`-`DB0` pins connect to the same-named
edge connector pins at positions `B26`-`B33`.

Pinout:

                           ┌─────┐
                       DI2 │9   8│ GND
                       DB2 │10  7│ DI1
                       DO2 │11  6│ DB1
                       DI3 │12  5│ DO1
                       DB3 │13  4│ DI0  CPU bus (tied to DO0)
     0 = DI → DB ─┐    DO3 │14  3│ DB0  expansion edge connector
     1 = DB → DO ─┴─ /DIEN │15  2│ DO0  CPU bus (tied to DI0)
                       Vcc │16  1│ /CS  1=HiZ
                           └──∩──┘


Substitution
------------

It should also be possible to substitute a 74LS245 or 74HCT245 with an
appropriate adapter board. The following shows a block diagram of the 8216,
how one can wire up a '245 to substitute for it in this application (this
is not a general substitution), and the spacing constraints for PCBs that
would drop into IC24 and IC25 sockets on the motherboard.

<img src='./8216-clone.jpg' height=600>

Thanks to [Edoardo/kinmami][kin] we have a design for a small PCB doing
just this. It uses a DIP 74HCT245N or 74LS245; and, to save space, eight
0805 SMT resistors and an 0805 capacitor.
- Photos, the schematic and BOM are available on the [project page on
  PCBWay][pcbway], and you can also order PCBs directly from there.
- This repo has local copies of:
  - The [schematic](./artwork/8216_sch.png)
  - [Board previews](./artwork/)
  - [EagleCAD source files](./EagleCAD)
  - [Gerbers](./Gerbers)

__NOTE:__ There are older versions of this project, one of which has a bug
in the layout: pin 19 on the '245 was not connected to pin 1 on the socket
interface. This is fixed in the latest revision, which also moves the
bypass cap to the resistor side of the board to make installing the '245
socket easier.

<img src='artwork/8216_sch.png' height=300>     <img
     src='artwork/8216_top.png' height=300>



<!-------------------------------------------------------------------->
[kin]: https://www.pcbway.com/project/member/?bmbno=74CA1DF4-D529-4B
[pcbway]: https://www.pcbway.com/project/shareproject/Training_Micro_computer_NEC_TK_85_replacement_NTE8126_74HCT245_Octal_bus_transce_f5f133b5.html
[μPB8216]: ../../datasheet/nte8216.pdf
