NEC TK-8x ROMs
==============

### TK-80

`tk80.bin` is the concatenation of `tk80-[123].bin` from
[`mame-merged/TK80.zip`][mm-dl] in the [MAME 0.261 ROM Set (Merged)][mm-ar]
on archive.org.

### TK-85

These are the ROM markings we've seen:

    NEC    PIY069
    D2316EC₇₀₂  TK85

    NEC    8421PN
    D2316EC₇₀₂  TK85

First 16 bytes the same for both:

    0000 3E 92      ld   A,$92
    0002 D3 FB      out  $F3
    0004 C3 5F 00   jmp  $005F
    0007 00         db   0
    0008 C3 B1 83   jmp  $83B1
    000B 00 00 00   db   0 rep 5
    000E 00 00



<!-------------------------------------------------------------------->
[mm-ar]: https://archive.org/details/mame-merged
[mm-dl]: https://archive.org/download/mame-merged
