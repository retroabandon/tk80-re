#   Comments file for TK85.rom disassembly, in sed(1) -E form
#   Assumes you've run z80dasm with with -a.
#   https://www.gnu.org/software/sed/manual/sed.html

/org/a\

/^segments\t/{s/\t//;s/$/\t\t; leftmost 7-segment display/}
/^segments\+7\t/ {
    s/^/;/          # comment out line; real assembler will do the calculation
    s/\t   //;
    s/$/\t\t; rightmost 7-segment display/
}

/^jpcoldstart/s/\t   //
s/\t;0004//

s/\t;003c/\n/

/^coldstart/s/\t //
s/\t;005e/\n/

/\t;00aa/ { s//\n/; a\
;   XXX unknown table of addresses to which we jump
}
s/\t;00b9/;???\n/

s/\t;010f/\n/

s/\t;0120/\t; MSB of offset is 0/
s/\t;0122/; word offset/

s/\t;0147/\n/
s/\t;0156/\n/

s/\t;0179/\n/
/^sub_017C/s/\t//

/\t;05e8/ { s//\n/; a\
;   ??? Clear a couple of housekeeping locations; return A=1
}
/^clrDBDE/ { s/rst00/$0000/ }

/\t;05f4/ { s//\n/; a\
;   Given a bit number 0-7 in A, return A with that bit set ($01, $02, $04 etc.)
}
s/\t;05f9/; bit 7 → bit 0/
s/\t;05f[567ab]//

s/\t;05fe/\n/

s/\t;07ab/\n/
s/\t;07bc/\n/
s/\t;07c5/\n/
s/\t;07ce/\n/
#/^_L_7E7/s/\t  //

    /;07f4/{ s/;07f4/\n/; a\
;   Store upper/lower nybble of A in (DE) and decrement DE.
}
/^stded_lsnyb/s/\t   //
s/\t;07f7/; shift right four bits/
s/\t;07f[89]//
/;07fa/{ s/;07fa//; a\
            ; fallthrough
}
/^stded_msnyb/s/\t   //
s/;07fb/; mask lower nybble/
s/\t;07f[def]//

####################################################################
#   Generic changes

s/ defw / dw    /
s/ defb / db    /
/^\t    db    \$20/s/.*/\t    rim/
/^\t    db    \$30/s/.*/\t    sim/
