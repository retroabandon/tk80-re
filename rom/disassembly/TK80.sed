#   Comments file for TK80.rom disassembly, in sed(1) -E form
#   Assumes you've run z80dasm with with -a.
#   https://www.gnu.org/software/sed/manual/sed.html

/org/a\

/^segments\t/{s/\t//;s/$/\t\t; leftmost 7-segment display/}

####################################################################
#   Generic changes

s/ defw / dw    /
s/ defb / db    /
