;   ASL setup to assemble `rom/TK85-orig.asm`.
;
;   The original source code in TK85-orig.asm was scanned from pages
;   125-139 of _TK-85 Training Book,_ by ForNext[1]. Here it has been
;   modified from that as minimally as possible, except to correct
;   typographic errors.
;
;   [1]: https://forum.vcfed.org/index.php?members/fornext.66471/
;

            relaxed on              ; accept $nn hex number syntax
            cpu 8085
            z80syntax off

            include "rom/TK85-orig.asm"
