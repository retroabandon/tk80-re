NEC TK-80BS Clone ROMs for MAME
===============================

These are Kamafusa Ichigo's [NEC TK-80BS Clone ROMs](../README.md) renamed
for use with MAME. Other than the names and the additional `tk80.dummy`
file, they are identical.

These we downloaded from the [planetemu.net TK-80BS MAME set][pemu]. They
are described in further detail on the [MAME Machine page for the
TK-80BS][mamemach] page and [in the source code][mamesrc], which mentions
that these are _not_ dumps from an original machine.

There were three versions of the various ROM sets: Level 1, Level 2 1.0 and
Level 2 1.1. (Which file is which should be clear from the names here:
`l1`, `10`, `11`.)

- `tk80.dummy` ($0000-$03FF): A "dummy" ROM for $0000-$03FF that contains
  only an RST0 (reset) vector jump to the BSMON ROM entrypoint ($F000) and
  an RST7 (hardware interrupt) vector to jump to the standard place in RAM
  used by BSMON ($83DD).
- `bsmon.{l1,10,11}` ($F000-$FFFF): BASIC STATION ROM monitor.
- `ext.{l1,10,11}` ($0C00-$7FFF)
- `lv1basic.l1` ($E000-$EFFF)
- `lv2basic.{10,11}` ($D000-$EFFF)
- `font.rom` (chargen region, $0000-$0FFF)


<!-------------------------------------------------------------------->
[mamemach]: https://mame.spludlow.co.uk/Machine.aspx?name=tk80bs
[mamesrc]: https://github.com/mamedev/mame/blob/mame0267/src/mame/nec/tk80bs.cpp
[pemu]: https://www.planetemu.net/rom/mame-roms/tk80bs
