NEC TK-80BS Clone ROMs
======================

This is a set of ROMs for the TK-80BS (or, more often, emulators) that
appears to be a clean-room reimplementation compatible with the original
NEC ROMs. It was developed by Kamafusa Ichigo and is distributed on Toshiya
Takeda's [eTK-80BS page][eTK] in three versions:
- `etkroms/`:  Level 1 BASIC         ([`etkroms.zip`])
- `etkroms2/`: Level 2 BASIC         ([`etkroms2.zip`])
- `etkroms3/`: Ver.3 Level 1/2 BASIC ([`etkroms3.zip`])

`src/` contains disassembled/reverse-engineered code for certain parts of
the clone ROMs. See [`src/README.md`](./src/README.md) for details.

The following are the original ROM names (which are not always directly
usable with the [CSCP emulator]), the corresponding names of the MAME
copies, and the SHA1 hashes of each file.

    80bs-clone/             …/mame/         SHA1
    ────────────────────────────────────────────────────────────────────────────────
     etkroms/BSM.ROM        bsmon.l1        7e6ec5bfb3eea114f7ee9ef589a89246b8533b2f
     etkroms/Ext.rom        ext.l1          8544aa2cb58df9edf221f5be2cdafa248dd33828
     etkroms/L1CB.ROM       lv1basic.l1     528c9331740637e853c099e1739ecdea6dd200bc
    ────────────────────────────────────────────────────────────────────────────────
    etkroms2/BSMON.ROM      bsmon.10        79e2b5dc47b574b55375cbafffff144744093ec1
    etkroms2/EXT.ROM        ext.10          919180d5b34b981ab3dd8b2885d3c0933203f355
    etkroms2/LV2BASIC.ROM   lv2basic.10     5854c1be5fa78c1bfee365379495f14bc23e15e7
    etkroms2/FONT.ROM       font.rom        9fe741eab866b0c520d4108bccc6277172fa190c
    ────────────────────────────────────────────────────────────────────────────────
    etkroms3/bsmon.rom      bsmon.11        7c7eb5e5e4cf1e0021383bdfc192b88262aba6f5
    etkroms3/Ext.rom        ext.11          2ad70828348372328b76bac0fa93d3f6f17ade34
    etkroms3/LV2BASIC.ROM   lv2basic.11     9539409c876bce27d630fe47d07a4316d2ce09cb
    etkroms3/font.rom       font.rom        9fe741eab866b0c520d4108bccc6277172fa190c
    ────────────────────────────────────────────────────────────────────────────────
                            tk80.dummy      939350d7fa56ce567ddf393c9f4b9db6ebc18a2c
    ────────────────────────────────────────────────────────────────────────────────

### Low ROM Changes for TK-80BS

On the original hardware with the original ROMs, the TK-80 would always
start in its standard keypad/LED monitor mode, and you needed to key in
`F000 ADDR RUN` to initialise the BASIC SYSTEM monitor. The emulators and
their ROMs usually configure a BASIC SYSTEM monitor auto-start as follows.

Neither CSCP nor MAME use the standard $000-$7FF ROMs when emulating
the TK-80BS. Instead they fill only:
- The $0000 `RST 0` reset entry point with `JMP $F000`
- The $0038 `RST 7` hardware interrupt entry point with `JMP $83DD`

The MAME `tk80.dummy` ROM is all zeros except for the above values.

The CSCP emulator automatically fills these values into its memory based on
what ROMs are loaded:
- If `TK80.ROM` is empty or missing, it fills both entry points as above,
  giving the exact data as `tk80.dummy` below.
- If `BSMON.ROM` > 0 bytes, $0000 (`RST 0`) is left unchanged, but the
  hardware interrupt entry point $0038 (`RST 7`) is overwritten with
  `JMP $83DD` as above.
- For the exact code, see `src/vm/tk80bs/tk80bs.cpp` around line 160.


Clone ROM Details
-----------------

### BSMON.ROM

($F000-FFFF) The Basic Station interactive monitor is not supplied with V1
(or V2?), but only with V3. However, from the first version it does provide
some compatible with the ROM routines used at the time to enable running
some machine-language programs that used these.

### EXT.ROM

These ROM images, loaded in the $0C00-$7BFF range, are specifically for for
running this clone ROM set on the emulator; the actual TK-80BS has no ROM
on this range. The various documentation files seem to indicate that this
provides hardware (video and keyboard) initialisation and BASIC and other
extensions (e.g., a screen editor) that vary with the version of the clone
ROMs.

`etkroms*/ETKROMS.TXT` (which is the same in V1 and V2, but does not exist
in V3) indicates that `TK80.ROM` is patched to jump to initialisation
routines in this. This is not done by the emulator code (see above), so it
may be done by `GETROM.TK8` (see below), and thus wouldn't happen with an
original TK-80 ROM image. There are also some further changes involved in
this as explained in the section "(2)EXT.ROM（0C00-7BFF)" of that file.

### GETROM.TK8

All three of the clone ROM sets supply a program called `GETROM.TK8` (start
location $8000). This copies the system ROM ($0000-$02FF) to $8000, patches
certain addresses in it, and then displays `SAvEtk80` on the LEDs. This is
intended to be run on the 「復活！TK-80」emulator, and presumably at that
point you can ask the emulator to save its RAM to a file which will then
contain the original ROM contents.


Clone ROM Versions
------------------

### `etkroms/` (Clone V1)

This has Level 1 BASIC only and does not include the "BS monitor." It can be
run in three configurations:

1. The original `TK80.ROM` (which they do not supply, but we do), `EXT.ROM`,
   `BSMON.ROM`, and `LV1BASIC.ROM`. Because there is no BS monitor, you
   must use the keypad `SET ADDR` and `RUN` commands to start BASIC.
   - Executing $E000 will go directly to Level 1 BASIC. Unlike the
     original, there is no need to enter `RAM END` after starting it.
   - Executing $F000 will return you to the regular TK-80 ROM.

2. The original `TK80.ROM` and `EXT.ROM`. This does not allow running BASIC
   programs or any programs that call into the BASIC ROM, but does support
   machine-language programs that need to use TK-80BS hardware. (This is
   presumably video and keyboard hardware initialised by `EXT.ROM`.)

3. `BSMON.ROM` and `LV1BASIC.ROM`. This supports BASIC only (the emulator
   will start it directly), and offers no monitor access via either
   keyboard/video or keypad/LEDs. (However the BASIC `CALL nnnnH` command
   can be used to execute machine-language programs.)

### `etkroms2/` (Clone V2)

`LV2BASIC.ROM` is based on Palto Alto Tiny BASIC with many new features to
make it (mostly) compatible with NEC Level 2 BASIC. Before trying to start
BASIC make sure you use the Emulator to select Devices » Boot » Level2.

For information on the monitor commands, see [`doc/tk80bs-mon.md`]

Loading a program (in Intel HEX format) - use the LOADH command, after it
asks READY? use the TAPE PLAY function in the emulator to specify the input
file. Then enter Y on the keyboard to continue.

### `etkroms3/` (Clone V3)



<!-------------------------------------------------------------------->
[CSCP emulator]: ../../emu/
[`doc/tk80bs-mon.md`]: ../../doc/tk80bs-mon.md
[`etkroms.zip`]: http://takeda-toshiya.my.coocan.jp/tk80bs/etkroms.zip
[`etkroms2.zip`]: http://takeda-toshiya.my.coocan.jp/tk80bs/etkroms2.zip
[`etkroms3.zip`]: http://takeda-toshiya.my.coocan.jp/tk80bs/etkroms3.zip
[eTK-80/85 WIP]: http://takeda-toshiya.my.coocan.jp/tk80bs/index.html
[eTK]: http://takeda-toshiya.my.coocan.jp/tk80bs/
