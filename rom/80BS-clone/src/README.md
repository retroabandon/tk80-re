NEC TK-80BS Clone ROM Disassembly
=================================

This is not the original source code for Kamafusa Ichigo's "clone" TK-80BS
ROMs (see [`REAMDE.md`](../README.md) in the directory above), but merely
disassembled and reverse-engineeered parts of them.
