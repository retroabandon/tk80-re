Sharp SM-B-80T / SM-B-80TE
==========================

Sharp's first 8-bit trainer board. Z80 CPU.

- `LH-8H07-monitor.bin`: SM-B-80TE monitor ROM (2K 2716).
