Non-NEC Trainer Boards
======================

This is a list of (and some information about) trainer boards similar to
the TK-80/TK-80BS/TK-85. (In some cases the similiarities may be very
distant.)

- [ICS-MTL](./ICS-MTL/)
- [MMD-2](./MMD-2/)
- [MP-85](./MP-85/)
- [MST-80](./MST-80/)
