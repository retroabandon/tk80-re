MITEK MP-85 (8085)
==================

- Interface:
  - 6× 7-segment LED. 25-key keypad.
  - CMT ear/mic/remote; Remote control on/off switch
  - 2× DIP switches (off,off when purchased) for ROM boot selection?
- Components:
  - Mitsubishi M5L8085AP
  - Mitsubishi M5L8255AP-5
  - NEC [D8279C-5] programmable keyboard/display driver
  - Mitsubishi M5L2716 2K×8 ROM "MP-85 MONITOR MITEC"
    - 3× pads for more 2716
    - 2× DIP switch (off, off when purchased), for ROM boot selection?
  - 2× M5L2114 1K×4 RAM (socketed DIP-18N)


The the [Shizu 2021] YouTube channel has some videos on this, including
constructing a keyboard and running TinyBASIC on it. He also has a copy
of the manual.
