Lawrence Livermore MST-80
=========================

The MST-80 Microprocessor Trainer, from Lawrence Livermore Laboratory,
was released 1976-05-21, according to the documentation.

The documentation here can also be found online at archive.org:
- [MST-80 Microprocessor Trainer][man]. Manual, with cover page.
  1976-05-21. 29 pp.
- [MST-80B Microcomputer Trainer Manual][man]. A different version of the
  manual, without cover page. 28 pp. Also includes a schematic as a
  separate PDF.
- [MST 80 B Microcomputer Trainer Schematic][man2sch]. The same schematic
  as above.

The archive.org items also have large, higher-res versions of the schematics.



<!-------------------------------------------------------------------->
[man]: https://ia802602.us.archive.org/view_archive.php?archive=/2/items/lawrence-livermore-labs-mst-80/Lawrence-Livermore-Labs-MST-80.zip&file=MST-80%2FManuals%2FMST-80B-May-21st-1976.pdf
[man2]: https://archive.org/details/MST80BMicrocomputerTrainerManual
[man2sch]: https://archive.org/details/MST80BMicrocomputerTrainerSchematic
