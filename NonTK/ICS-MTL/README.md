Integrated Computer Systems Microprocessor Training Laboratory
==============================================================

The Microprocessor Training Laboratory (MTL) from Integrated Computer
Systems (ICS) consists of the following components:
- MTS - Microprocessor Training System
  - Trainer board with 25-key hex keypad and 8-digit 7-segment display.
  - 8080A CPU, 1K PROM (expandable to 8K), 2K RAM (expandable to 4K), CMT
    interface.
  - 7-segment display uses same DMA system as TK-80, even at the same
    addresses ($83F8-$83FF).
- ITS - Interface Training System
  - PCB with I/O ports, A/D converter, timers, vectored priority interrupt
    conroller.
- Integrated Experiment Assembly
  - Smaller PCB with speaker, thermistor, motor and switches and pots.
- Power Supply

This system seems to have some substantial similarities to the TK-80 (note
the DM-based display above). We're awaiting a ROM dump to disassemble it to
see how similar it really is.

### References

- [Bitsavers documentation]
- [VCF Forum Thread]



<!-------------------------------------------------------------------->
[Bitsavers documentation]: https://bitsavers.org/pdf/integratedComputerSystems/
[VCF Forum Thread]: https://forum.vcfed.org/index.php?threads/8080-sbc-integrated-computer-systems.50752/
