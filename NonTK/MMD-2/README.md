E&L Instruments MMD-1/MMD-2
===========================

E&L Instruments also published considerable technical documentation and
tutorials for all the following boards.

### MMD-1

The MMD-1 Mini-Micro Designer, from E&L Instruments Limited in Connecticut,
was introduced as the "Dyna-Micro 8080 Computer" in _Radio-Electronics_
magazine in the [May 1976][re5]  and [June 1976][re6] issues. Archive.org
has the [MMD-1 Manual][man1].

It has an 2 MHz 8080A, 512 bytes of ROM for keyboard input and CMT I/O, 512
bytes of RAM, 24 LEDs, a 16-key keypad (octal input), and a built-in
breadboard and power supply.

### MMD-2

The MMD-2 was released in July 1981.

It has a 2 MHz 8080A, 2-8K ROM (2708/2716), 256b "scratchpad" + 1-4K RAM,
serial port for RS-232/CMT, 2× 16-key keypads, 9× 7-segment displays (split
octal), 3× 8-leds, EPROM programmer, [STD bus] interface.
connector).

References:
- pestingers.net, [E&L MMD2][pes]. Photos, manuals and other documentation.
- YouTube, Play with Junk [PWJ78]. Video covering the MMD-2.

EPROM images:
- `330EXEC_1.bin`: base operating system.
- `340EXEC.bin`
- `350ELEA.bin`
- `360ELEB.bin`


<!-------------------------------------------------------------------->
[PWJ78]: https://www.youtube.com/watch?v=5q4m5hk4lpk
[STD bus]: https://en.wikipedia.org/wiki/STD_Bus
[man1]: https://archive.org/details/manual_revl
[pes]: https://www.pestingers.net/pages-images/antique-computers/e&l-mmd2/e&l-mmd2.htm
[re5]: https://archive.org/details/radioelectronics47unse_3
[re6]: https://archive.org/details/radioelectronics47unse_4
