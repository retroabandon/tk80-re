TK-80 and TK-85 Emulation
=========================

The `emu` script in this directory will start the CSCP TK-80, TK-80BS or
TK-85 emulator. See [`CSCP.md`](./CSCP.md) for details.

To extract the files from the CSCP download you'll need a program that
support 7z format. The Python `patoolib` package tries several options:
- For Windows we suggest you install [7-Zip].
- For Debian-based Linux we suggest `apt-get install 7zip`.

On Linux this requires the 32-bit version of Wine. On Debian-based systems,
install it with:

    sudo dpkg --add-architecture i386   # If you've not done this before.
    sudo apt-get update
    sudo apt-get install wine32:i386
    #   If you've ever run the 64-bit version of Wine before installing
    #   the 32-bit version, the 32-bit version won't run until Wine
    #   rebuilds your configuration, which the following will trigger.
    rm -rf ~/.wine/


List of Emulators
-----------------

- MAME emulates the [TK-80][mame-tk80], [ICS-8080][mame-ics8080],
  [Mikrolab KR580IK80][mame-mikrolab] (Russian), [TK-85][mame-nectk85], and
  [ND-80Z][mame-nd80z] (Z80-based). The system ROM for the TK-80 is in the
  [MAME 0.261 ROM Set](rom/README.md). The driver source,
  [`trainer/tk80.cpp`][mamedrv], is small and easy to read and gives some
  useful hardware information about all of these systems.
- The [Common Source Code Project][cscp] has emulators for a vast number
  of Japanese microcomputer systems, including the [TK-80, TK-80BS and
  TK-85][cscp-eTK].
- `Tk80.exe`/TK-80シミュレータ by Masanori Sakaki is an emulator for
  Windows 95/98/NT4.0 (but confirmed to work on Windows 10) written in 2000
  using MS Visual C++ 6.0 with MFC. The binary and source code are included
  on the CD-ROM that came with the book 「復活TK-80」 (_TK-80 Revivial_),
  which is not otherwise publicly available.
- [U80][u80emu] emulates various configurations of the TK-80/TK-80E/TK-80BS
  (including extended memory) and the TK-85. The package does not include
  ROM images. It's also distributed as obfuscated Java `.class` files, with
  no source.
- ForNextSoftwareDevelopment's [TK-85][fnsw] is a Windows (C#) TK-85
  simulator, assembler, disassembler and debugger as a visual IDE-style
  program. It's too slow to run programs at full TK-85 speed, however.
- [TK-80BS Emulator for CH32V203][ch32v203]. Runs on a 96 MHz CH32V203C8
  microcontroller board, and should run almost as fast as the real thing.
  Uses composite (?) for TK-80BS television output and a USB keyboard for input.
  Also supports a TK-80 mode, where they keyboard emulates a TK-80 keypad.
  This may be based on MAME code, and uses hardware similar to his
  [JR-100 emulator].
- The [ミニ版ZK-80組立てキット ATD-B-001S][atd] is a very cute little
  (80×60×12 mm) and cheap (¥2500) hardware emulator using a PIC32MX120F032B.
  Unfortunately it seems out of production and unavailable now.
  [Schematic and source code.][atd-sch]



<!-------------------------------------------------------------------->
[7-Zip]: https://www.7-zip.org/

<!-- List of Emulators -->
[mame-ics8080]: http://adb.arcadeitalia.net/dettaglio_mame.php?game_name=ics8080
[mame-mikrolab]: http://adb.arcadeitalia.net/dettaglio_mame.php?game_name=mikrolab
[mame-nd80z]: http://adb.arcadeitalia.net/dettaglio_mame.php?game_name=nd80z
[mame-nectk85]: http://adb.arcadeitalia.net/dettaglio_mame.php?game_name=nectk85
[mame-tk80]: http://adb.arcadeitalia.net/dettaglio_mame.php?game_name=Tk80
[mamedrv]: http://adb.arcadeitalia.net/download_file.php?tipo=sourcefile&codice=tk80&emulator=978&oper=view

[cscp]: http://takeda-toshiya.my.coocan.jp/common/index.html
[cscp-eTK]: http://takeda-toshiya.my.coocan.jp/tk80bs/index.html

[u80emu]: https://emutopia.com/index.php/emulators/item/412-nec-tk-80-tk-85/1266-u80

[fnsw]: https://github.com/ForNextSoftwareDevelopment/TK-85

[ch32v203]: https://github.com/shippoiincho/TK80BSEmulator
[jr-100 emulator]: https://github.com/shippoiincho/jr100emulator_ch32v203

[atd-sch]: https://www.recfor.net/blog/mycom/?itemid=883
[atd]: http://physics.cocolog-nifty.com/weblog/2020/07/post-98dea2.html
