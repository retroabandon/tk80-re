#!/usr/bin/env bash
set -Eeuo pipefail

export T8_PROJDIR=$(cd "$(dirname "$0")/.." && pwd -P)
. "$T8_PROJDIR/pactivate" -q

usage() { echo "Usage: $(basename "$0") tk80|tk80bs|tk85 [ROM=...]"; }

badarg() { echo 1>&2 "ERROR:" "$@"; usage; exit 2; }

[[ $# -lt 1 ]] && badarg "argument required"
case "$1" in
    tk80)   shift
            emulator=tk80
            roms=(
                TK80.ROM="$T8_PROJDIR/rom/TK80.bin"
                EXT.ROM=/dev/null
                BSMON.ROM=/dev/null
            )
            ;;
    tk80bs) shift
            emulator=tk80bs
            roms=(
                TK80.ROM=/dev/null
                EXT.ROM="$T8_PROJDIR/rom/80BS-clone/etkroms3/Ext.rom"
                BSMON.ROM="$T8_PROJDIR/rom/80BS-clone/etkroms3/bsmon.rom"
                LV1BASIC.ROM="$T8_PROJDIR/rom/80BS-clone/etkroms/L1CB.ROM"
                LV2BASIC.ROM="$T8_PROJDIR/rom/80BS-clone/etkroms3/LV2BASIC.ROM"
                FONT.ROM="$T8_PROJDIR/rom/80BS-clone/etkroms3/font.rom"
            )
            ;;
    tk85)   shift
            emulator=tk85
            roms=(
                TK85.ROM="$T8_PROJDIR/rom/TK85.bin"
                EXT.ROM=/dev/null
            )
            ;;
    -h)     usage; exit 0;;
    *)      badarg "Unknown emulator: '$1'"
esac
roms+=("$@")

echo '===== Checking CSCP emulators'
t8dev buildtoolset cscp

echo "===== Running emulator $emulator"
exec t8dev emulator cscp "$emulator" "${roms[@]}"
