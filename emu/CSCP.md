CSCP TK-80 and TK-85 Emulators
==============================

The `emu` script in this directory will start the CSCP TK-80 TK-85BS or
TK-85 emulator using a default set of ROMs from this repository. The first
parameter must be the system to emulate (`tk80`, `tk80bs` or `tk85`).


Keyboard
--------

The key mappings for keyboard input on the hex keypad are as follows:

      PC        TK-80       TK-85
    ─────────────────────────────────
      0-9A-F      (Digit Keys)
    ─────────────────────────────────
          F1    RET         CONT
          F2    RUN         RUN
          F3    STORE DATA  MODE
          F4    LOAD DATA   REG
    ─────────────────────────────────
          F5    ADRS SET    ADRS SET
     PgUp F6    READ INCR   READ INC
     PgDn F7    READ DECR   READ DEC
    Enter F8    WRITE INCR  WR/ENT
    ─────────────────────────────────


Loading Alternate ROM Images
----------------------------

You can supply additional parameters in the form `NAME=path`, where _NAME_
is the name of the ROM file as loaded by the emulator and _path_ is the
path to a ROM image or a URL from which to download a ROM image. So e.g.,
`emu/emu tk80 EXT.ROM=foo.bin` will start the TK-80 emulator with the
contents of `foo.bin` loaded at location $0C00.

For the TK-80, the ROM locations that are loaded for _NAME_ above are:

    TK80.ROM        $0000-$07FF
    EXT.ROM         $0C00-$7BFF
    BSMON.ROM       $F000-$FFFF
    LV1BASIC.ROM    $E000-$EFFF
    LV2BASIC.ROM    $D000-$EFFF
    FONT.ROM        (separate address space)

For the TK-85:

    TK85.ROM        $0000-$07FF
    EXT.ROM         $0C00-$7BFF

Additionally, you may specify an `@nnnn:` before _file,_ where _nnnn_ is
hex digits, to load the ROM offset from the ROM's start location. E.g.,
specifying `TK80.ROM=@0040:myfile.bin`, with `myfile.bin` being 64 bytes
long, will overwrite locations $40 through $7F in the standard ROM with the
contents of `myfile.bin`.


Loading and Saving RAM Images
-----------------------------

The TK-80 and TK-85 have a "RAM" menu with "Load" and "Save" options; these
load and save memory starting at $8000. These can be used to load the
`.ram` images under `bin.tk80/` and `bin.tk85/`. For example, if you load
`bin.tk85/charset.ram` and press `8 2 2 0 ADDR RUN` it will display the
TK-85 character set eight chars at a time. (Press any number key to move to
the next group of 8.)

Saving will save all RAM from $8000 through $CFFF.


Loading and Saving Tape Images
------------------------------

### TK-85

The [CMT Load/Save](../doc/tk85-mon.md#cmt-loadsave) instructions in
`doc/tk85-mon.md` cover the load and save procedures for the TK-85 monitor.
In the emulator, you will need to do the following:
1. From the menus, Use __CMT » Play__ to "insert" the cassette tape by
   chosing the file containing the tape image(s).
2. Set up the TK-85 to load from tape, and start it loading.
3. Use __CMT » Play Button__ to play the tape image; you will hear the tape
   noises as it plays.
4. The TK-85 will display the load start/end addresses or, if you got the
   file number wrong, the file number it most recently read.
4. To re-try the load (perhaps with a different file number), use __CMT »
   Fast Rewind__ to rewind the tape and start again from step 2.
